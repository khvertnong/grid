import 'package:flutter/material.dart';
class MenuModel{
  int? id;
  String? name;
  Icon? icon;
  String? route;

  MenuModel({this.id, this.name, this.icon, this.route});
}
List<MenuModel> drawerList =[
  MenuModel(id: 1, name: "Account", icon: Icon(Icons.account_circle),route: ""),
  MenuModel(id: 2, name: " Local trasfer", icon: Icon(Icons.person),route: ""),
  MenuModel(id: 3, name: "World transfer", icon: Icon(Icons.account_balance_wallet_sharp),route: ""),
  MenuModel(id: 4, name: "Bill payment", icon: Icon(Icons.payment),route: ""),
  MenuModel(id: 5, name: "Phone Top up", icon: Icon(Icons.phone),route: ""),
  MenuModel(id: 6, name: "Code to Bank", icon: Icon(Icons.code),route: ""),
  MenuModel(id: 7, name: "Saving", icon: Icon(Icons.save),route: ""),
  MenuModel(id: 8, name: "Loan", icon: Icon(Icons.money),route: ""),
  MenuModel(id: 9, name: "Cash out", icon: Icon(Icons.attach_money_outlined),route: ""),
];