import 'package:flutter/material.dart';

class MenuButton{
  int? id;
  String? name;
  Icon? icon;
  String? route;

  MenuButton({this.id, this.name, this.icon, this.route});
}
List<MenuButton> homeMenuButton =[
  MenuButton(id: 1, name: "Account", icon: Icon(Icons.food_bank_outlined), route: ""),
  MenuButton(id: 2, name: "Local Transfer", icon: Icon(Icons.transfer_within_a_station), route: ""),
  MenuButton(id: 3, name: "World Transfer", icon: Icon(Icons.transfer_within_a_station), route: ""),
  MenuButton(id: 4, name: "Bill Payment", icon: Icon(Icons.payment), route: ""),
  MenuButton(id: 5, name: "Bill to Up", icon: Icon(Icons.phone_android_sharp), route: ""),
  MenuButton(id: 6, name: "Code to Bank", icon: Icon(Icons.phone_android_sharp), route: ""),
  MenuButton(id: 7, name: "Saving", icon: Icon(Icons.savings), route: ""),
  MenuButton(id: 8, name: "Loans", icon: Icon(Icons.local_activity), route: ""),
  MenuButton(id: 9, name: "Cash-Out", icon: Icon(Icons.money_outlined), route: ""),
];