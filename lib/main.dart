import 'package:flutter/material.dart';
import 'package:grid_ui/screen/home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cambodia Bank',
      theme: ThemeData(
          primarySwatch: Colors.deepPurple
      ),
      initialRoute: "/",
      routes: {
        "/": (context)=> HomeScreen(),
      },
    );
  }
}