import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grid_ui/menu/model_menu.dart';

import '../menu/menu_button.dart';
import '../widget/aba_text_logo.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar,
      drawer: Drawer(
        child: ListView(
          children: [
            Container(
              color: Colors.blueGrey,
              child: DrawerHeader(
                  child: Column(
                children: [
                  CircleAvatar(
                    backgroundImage: NetworkImage(""),
                  ),
                  Text(
                    "Name",
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "Phone",
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              )),
            ),
            Container(
              height: double.maxFinite,
              child: ListView.builder(
                  itemCount: drawerList.length,
                  itemBuilder: (BuildContext context, index) {
                    var menu = drawerList[index];
                    return ListTile(
                      onTap: () {
                        if (menu.route == "/") {
                          return Navigator.pop(context);
                        }
                        Navigator.pop(context);
                        Navigator.pushNamed(context, menu.route!);
                      },
                      leading: menu.icon,
                      title: Text("${menu.name}"),
                    );
                  }),
            )
          ],
        ),
      ),
      body: _buildBody,
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            backgroundColor: Colors.black26,
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.qr_code),
            label: 'Business',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.location_on),
            label: 'School',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
      ),
    );
  }

  AppBar get _buildAppBar {
    return AppBar(
      title: abaTextLogo,
      actions: [
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.language,
              size: 30,
            )),
        IconButton(onPressed: () {}, icon: Icon(Icons.settings)),
      ],
    );
  }

  Widget get _buildBody {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(2),
      margin: EdgeInsets.all(2),
      child: Column(
        children: [
          Flexible(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 2.0),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  gradient: RadialGradient(
                    colors: [
                      Colors.white,
                      Colors.black26,
                    ],
                  ),
                ),
                child: GridView.count(
                  crossAxisCount: 3,
                  mainAxisSpacing: 3,
                  crossAxisSpacing: 3,
                  children: List.generate(homeMenuButton.length, (index) {
                    var homeMenu = homeMenuButton[index];
                    return Container(
                      padding: EdgeInsets.all(2),
                      margin: EdgeInsets.all(5),
                      color: Colors.blueGrey,
                      child: Container(
                        color: Colors.black26,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              onPressed: () {},
                              icon: homeMenu.icon!,
                              iconSize: 40,
                              color: Colors.white,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${homeMenu.name}",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
                ),
              )),
        ],
      ),
    );
  }
}
