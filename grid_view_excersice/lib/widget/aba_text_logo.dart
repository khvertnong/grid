import 'package:flutter/material.dart';

Widget get abaTextLogo{
  return Row(
    children: [
      Text("ABA",style: TextStyle(letterSpacing: 2),),
      Text("'",style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),),
      SizedBox(width: 4,),
      Text("Mobile")
    ],
  );
}
