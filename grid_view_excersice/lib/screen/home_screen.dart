import 'package:abu_ui/menu/drawer_menu.dart';
import 'package:abu_ui/menu/menu_button.dart';
import 'package:abu_ui/widget/aba_text_logo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar,
      drawer: Drawer(
        child: ListView(
          children: [
            Container(
              color: Color(0xFF005D86),
              child: DrawerHeader(
                  child: Column(
                children: [
                  CircleAvatar(
                    backgroundImage: NetworkImage("https://scontent.fpnh12-1.fna.fbcdn.net/v/t1.6435-9/105554442_1168232633535419_6795135606067187641_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeHnScX2i1f2sM3nOLerloxGZn_yLa-NwOVmf_Itr43A5cNLZSjPIiyWp_xRmBJFJGzJfVmr4r6bA0MIpgI-0Fs3&_nc_ohc=5WA7JmfjduMAX8Wcu6E&_nc_ht=scontent.fpnh12-1.fna&oh=00_AfAixCOJH3Qow-urK6iVbydjhOXSIsIXt8YLyP-grjViTg&oe=637FAFE8"),
                  ),
                  Text(
                    "Name",
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "Phone",
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              )),
            ),
            Container(
              height: double.maxFinite,
              child: ListView.builder(
                  itemCount: drawerList.length,
                  itemBuilder: (BuildContext context, index) {
                    var menu = drawerList[index];
                    return ListTile(
                      onTap: () {
                        if (menu.route == "/") {
                          return Navigator.pop(context);
                        }
                        Navigator.pop(context);
                        Navigator.pushNamed(context, menu.route!);
                      },
                      leading: menu.icon,
                      title: Text("${menu.name}"),
                    );
                  }),
            )
          ],
        ),
      ),
      body: _buildBody,
    );
  }

  AppBar get _buildAppBar {
    return AppBar(
      title: abaTextLogo,
      actions: [
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.notifications_none_outlined,
              size: 30,
            )),
        IconButton(onPressed: () {}, icon: Icon(Icons.phone_in_talk_outlined)),
        IconButton(onPressed: () {}, icon: Icon(Icons.qr_code_scanner_rounded)),
      ],
    );
  }

  Widget get _buildBody {
    return Container(
      child: Column(
        children: [
          Flexible(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  gradient: RadialGradient(
                    colors: [
                      Colors.white,
                      Color(0xFF004469),
                    ],
                  ),
                ),
                child: GridView.count(
                  crossAxisCount: 3,
                  mainAxisSpacing: 1,
                  crossAxisSpacing: 1,
                  children: List.generate(homeMenuButton.length, (index) {
                    var homeMenu = homeMenuButton[index];
                    return Container(
                      color: Color(0xFF004469),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            onPressed: () {},
                            icon: homeMenu.icon!,
                            iconSize: 40,
                            color: Colors.white,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "${homeMenu.name}",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    );
                  }),
                ),
              )),
          Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.only(left: 20),
                width: double.infinity,
                color: Color(0xFF00BCD5),
                child: Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Quick Transfer',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 15)),
                        SizedBox(height: 7),
                        Text(
                            'Create your templates here to make \ntransfer quicker',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      ],
                    ),
                    Positioned(
                        right: -30,
                        bottom: -40,
                        child: Icon(
                          Icons.change_circle_outlined,
                          color: Colors.white.withOpacity(0.5),
                          size: 130,
                        ))
                  ],
                ),
              )),
          Flexible(
              flex: 1,
              child: Container(
                padding: EdgeInsets.only(left: 20),
                width: double.infinity,
                color: Color(0xFFEE5351),
                child: Stack(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Quick Payment",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        Text(
                          "Paying your bills with templates is faster",
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        )
                      ],
                    ),
                    Positioned(
                      right: -30,
                      bottom: -40,
                      child: Icon(
                        Icons.circle,
                        color: Colors.white.withOpacity(0.6),
                        size: 130,
                      ),
                    ),
                    Positioned(
                      right: -24,
                      bottom: -34,
                      child: Icon(
                        Icons.circle,
                        color: Color(0xFFEE5351).withOpacity(0.7),
                        size: 115,
                      ),
                    ),
                    Positioned(
                      right: -10,
                      bottom: -20,
                      child: Icon(
                        CupertinoIcons.money_dollar_circle_fill,
                        color: Colors.white.withOpacity(0.3),
                        size: 90.5,
                      ),
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
