import 'package:flutter/material.dart';

class MenuButton{
  int? id;
  Icon? icon;
  String? name;

  MenuButton({this.id, this.icon, this.name});
}
List<MenuButton> homeMenuButton =[
  MenuButton(id: 1, icon: Icon(Icons.account_balance_wallet),name: "Account"),
  MenuButton(id: 1, icon: Icon(Icons.credit_card),name: "Card"),
  MenuButton(id: 1, icon: Icon(Icons.monetization_on_outlined),name: "Payment"),
  MenuButton(id: 1, icon: Icon(Icons.fiber_new_rounded),name: "New Account"),
  MenuButton(id: 1, icon: Icon(Icons.e_mobiledata),name: "Cash to ATM"),
  MenuButton(id: 1, icon: Icon(Icons.repeat_outlined),name: "Transfer"),
  MenuButton(id: 1, icon: Icon(Icons.qr_code_scanner),name: "Scan QR"),
  MenuButton(id: 1, icon: Icon(Icons.attach_money_outlined),name: "Loans"),
  MenuButton(id: 1, icon: Icon(Icons.widgets_outlined),name: "Services"),

];