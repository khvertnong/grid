import 'package:flutter/material.dart';
class DrawerMenu{
  int? id;
  String? name;
  Icon? icon;
  String? route;

  DrawerMenu({this.id, this.name, this.icon, this.route});
}
List<DrawerMenu> drawerList =[
  DrawerMenu(id: 1, name: "Schedule Payment", icon: Icon(Icons.calendar_month),route: "/payment"),
  DrawerMenu(id: 2, name: " CheckBook", icon: Icon(Icons.check_box_rounded),route: "/checkbook"),
  DrawerMenu(id: 3, name: "ABA Places", icon: Icon(Icons.place_outlined),route: "/place"),
  DrawerMenu(id: 4, name: "Exchange Rates", icon: Icon(Icons.monetization_on_outlined),route: "/locator"),
  DrawerMenu(id: 5, name: "Locator", icon: Icon(Icons.location_searching),route: "/payment"),
  DrawerMenu(id: 6, name: "Invite Friends", icon: Icon(Icons.person_add_alt),route: "/invite"),
  DrawerMenu(id: 7, name: "Contact Us", icon: Icon(Icons.phone_in_talk_outlined),route: "/contact"),
  DrawerMenu(id: 8, name: "Term & Conditions", icon: Icon(Icons.terminal),route: "/term"),
  DrawerMenu(id: 9, name: "Settings", icon: Icon(Icons.settings),route: "/setting"),
];