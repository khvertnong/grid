import 'package:abu_ui/screen/home_screen.dart';
import 'package:abu_ui/screen/setting_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ABA Mobile',
      theme: ThemeData(
        primarySwatch: Colors.indigo
      ),
      initialRoute: "/",
      routes: {
        "/": (context)=> HomeScreen(),
        "/setting": (context) => SettingScreen(),
      },
    );
  }
}

